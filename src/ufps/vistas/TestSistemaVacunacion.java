/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.vistas;

import Negocio.SistemaNacionalVacunacion;

/**
 *
 * @author madar
 */
public class TestSistemaVacunacion {

    public static void main(String[] args) {
        SistemaNacionalVacunacion mySistema = new SistemaNacionalVacunacion();
        mySistema.setUrlDpto("https://gitlab.com/madarme/archivos-persistencia/raw/master/votaciones/departamento.csv");
        mySistema.setUrlMunicipio("https://gitlab.com/madarme/archivos-persistencia/raw/master/votaciones/municipios.csv");
        mySistema.setUrlPersona("https://gitlab.com/madarme/archivos-persistencia/raw/master/votaciones/personas.csv");
        mySistema.setUrlContenedor("https://gitlab.com/madarme/archivos-persistencia/-/raw/master/vacuna/vacuna.csv");
        mySistema.cargarDptos();
        mySistema.cargarMunicipios();
        mySistema.cargarPersonas();
        mySistema.cargarProveedores();
        System.out.println("Departamentos:" + mySistema.getDptos().toString());
        System.out.println("\n\n Mis Proveedores:" + mySistema.getListadoProveedores());
    }

}
