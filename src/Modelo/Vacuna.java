/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.time.LocalDate;

/**
 *
 * @author madarme
 */
public class Vacuna {
    
    private int id_vacuna;
    private LocalDate fecha_expiracion;

    public Vacuna() {
    }

    public Vacuna(int id_vacuna, LocalDate fecha_expiracion) {
        this.id_vacuna = id_vacuna;
        this.fecha_expiracion = fecha_expiracion;
    }

    public int getId_vacuna() {
        return id_vacuna;
    }

    public void setId_vacuna(int id_vacuna) {
        this.id_vacuna = id_vacuna;
    }

    public LocalDate getFecha_expiracion() {
        return fecha_expiracion;
    }

    public void setFecha_expiracion(LocalDate fecha_expiracion) {
        this.fecha_expiracion = fecha_expiracion;
    }

    @Override
    public String toString() {
        return "\n Vacuna{" + "id_vacuna=" + id_vacuna + ", fecha_expiracion=" + fecha_expiracion + '}';
    }
    
    
    
    
}
